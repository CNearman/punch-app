import React, {useState} from 'react';
import {View, TouchableHighlight, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {Portal, Modal, Title, Text, TextInput, HelperText, Button} from 'react-native-paper';
import {resetPunches} from '../redux/actions/resetPunchesAction';

const ResetPunchesModal = (props) => {

	return (
		<View>
			<View style={styles.inputContainer}>
				<Title>Reset Punches</Title>
				<Text style={{...styles.textSection, paddingBottom: 10}}>Are you sure you want to reset the punches for this instrument? This cannot be undone!</Text>
				<TouchableHighlight underlayColor="transparent">
		    	<Button 
		    		style={{...styles.button, alignSelf:"flex-end"}} 
		    		mode="contained" 
		    		onPress={() => {
		            	props.dispatch(resetPunches(props.currentInstrumentId))
		                props.hideModal();
		     		}}>
		         		Yes, I am sure.
		     		</Button>
		    	</TouchableHighlight>
			</View>
		</View>
	);
}

const styles = StyleSheet.create({
  button: {
  	marginTop: 100
  },
  textSection: {
  	paddingBottom: 10
  },
  input: {
  	marginBottom: 10,
  },
  inputContainer: {
  	flexDirection: "column",
  	justifyContent: 'space-around',
  	padding: 8,
  	backgroundColor: '#222'
  },
});

export default connect()(ResetPunchesModal);