import React, {useState} from 'react';
import {View, TouchableHighlight, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {Portal, Modal, Title, Text, TextInput, HelperText, Button} from 'react-native-paper';
import {savePunch} from '../redux/actions/savePunchAction';

import DynamicStackInfo from './DynamicStackInfo'

const validateIsNotANumber = (input) => {
	return isNaN(Number(input))
}

const initialState = {
	amount: "",
	price: ""
};

const AddPunchModal = (props) => {
	const [punch, setPunch] = useState({
		amount: "",
		price: ""
	})

	const resetState = () =>{
		setPunch(initialState);
	}

    let displayAmount = Number(punch.amount) || 0;
    let displayPrice = Number(punch.price) || 0;

  	let errors = [];
  	if (validateIsNotANumber(punch.amount)) {
  		errors.push('Amount');
  	}
  	if (validateIsNotANumber(punch.price)) {
  		errors.push('Price');
  	}

	return (
		<View>
			<View style={styles.inputContainer}>
				<Title>New Punch</Title>
				<TextInput
					label="Amount"
					keyboardType= 'number-pad'
					numeric
					value={punch.amount}
					onChangeText={text => setPunch({...punch, amount:text})}
					error={validateIsNotANumber(punch.amount)}
					style={styles.input}
				/>
				<TextInput
					label="Price"
					keyboardType= 'number-pad'
					numeric
					value={punch.price}
					onChangeText={text => setPunch({...punch, price:text})}
					error={validateIsNotANumber(punch.price)}
					style={styles.input}
				/>
				<HelperText type="error" visible={validateIsNotANumber(punch.amount) || validateIsNotANumber(punch.price)}>
					{errors.join(' and ').concat(`${errors.length > 1 ? " are" : " is" } not number${errors.length > 1 ? "s" : "" }!`)}
				</HelperText>
				<TouchableHighlight underlayColor="transparent">
		    	<Button 
		    		style={{...styles.button, width: 100, alignSelf:"flex-end"}} 
		    		mode="contained" 
		    		onPress={() => {
		            	if (displayAmount == 0 || displayPrice == 0) {
		            		return;
		            	}
		                var punchDetails = {};
		                punchDetails.amount = displayAmount;
		                punchDetails.price = displayPrice;
		                punchDetails.instrumentId = props.currentInstrumentId
		                props.dispatch(savePunch(punchDetails))
		                resetState();
		                props.hideModal();
		     		}}>
		         		Save
		     		</Button>
		    	</TouchableHighlight>

			<DynamicStackInfo 
				stackSize={ props.totalPunchCount + displayAmount }
				averagePrice={(props.totalCost + (displayAmount * displayPrice)) / (props.totalPunchCount + displayAmount) }
				/>
			</View>
		</View>
	);
}

const styles = StyleSheet.create({
  button: {
  },
  input: {
  	marginBottom: 10,
  },
  inputContainer: {
  	flexDirection: "column",
  	justifyContent: 'space-around',
  	padding: 8,
  	backgroundColor: '#222'
  },
});

export default connect()(AddPunchModal);