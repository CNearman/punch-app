import React from 'react';
import {View} from "react-native";
import {Text, Surface} from 'react-native-paper';

const CurrentStackData = (props: any) => {
	return (
		<Surface>
			<View style={{flexDirection: 'row', justifyContent:'space-around'}}>
				<View style={{flexDirection:'column', padding: 8,  alignItems: 'center', justifyContent: 'center'}}>
					<Text>Stack Size</Text>
					<Text style={{fontSize: 28}}>{props.stackSize}</Text>
				</View>				
				<View style={{flexDirection:'column', padding: 8, alignItems: 'center', justifyContent: 'center'}}>
					<Text>Avg. Stack Price</Text>
					<Text style={{fontSize: 28}}>{props.averageStackPrice}</Text>
				</View>
			</View>
		</Surface>
	);
}

export default CurrentStackData