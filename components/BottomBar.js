import React, {useState} from 'react';
import {BottomNavigation, Text} from 'react-native-paper';

import StackScreen from '../screens/stackScreen'

const StackRoute = () => <Text>Stack</Text>;

const GoalRoute = () => <Text>Goal</Text>;

const SettingsRoute = () => <Text>Settings</Text>;

const BottomBar = () => {
	const [index, setIndex] = useState(0)

	const [routes] = useState([
		{key: 'stack', title: 'Stack', icon: 'hamburger'},
		{key: 'goal', title: 'Goal Calculator', icon: 'album'},
		{key: 'settings', title: 'Settings', icon: 'history'},
	]);

	const renderScene = BottomNavigation.SceneMap({
		stack : StackScreen,
		goal: GoalRoute,
		settings : SettingsRoute
	})

	return (
		<BottomNavigation
			navigationState={{index, routes}}
			onIndexChange={setIndex}
			renderScene={renderScene}
		/>
	);
}

export default BottomBar;