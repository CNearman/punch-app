import React, {useState} from 'react';
import {View, TouchableHighlight, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {Portal, Modal, Title, Text, TextInput, Button} from 'react-native-paper';
import {addInstrument} from '../redux/actions/addInstrumentAction';

import DynamicStackInfo from './DynamicStackInfo'


const initialState = {
	name: ""
};

const AddInstrumentModal = (props) => {
	const [instrument, setInstrument] = useState({
		name: ""
	})

	const resetState = () =>{
		setInstrument(initialState);
	}

	return (
		<View>
			<View style={styles.inputContainer}>
				<Title>New Instrument</Title>
				<TextInput
					label="Name"
					value={instrument.name}
					onChangeText={text => setInstrument({...instrument, name:text})}
					style={styles.input}
				/>
				<TouchableHighlight underlayColor="transparent">
		    	<Button 
		    		style={{...styles.button, width: 100, alignSelf:"flex-end"}} 
		    		mode="contained" 
		    		onPress={() => {
		                props.dispatch(addInstrument(instrument))
		                resetState();
		                props.hideModal();
		     		}}>
		         		Add
		     		</Button>
		    	</TouchableHighlight>		
			</View>
		</View>
	);
}

const styles = StyleSheet.create({
  button: {
  },
  input: {
  	marginBottom: 10,
  },
  inputContainer: {
  	flexDirection: "column",
  	justifyContent: 'space-around',
  	padding: 8,
  	backgroundColor: '#222'
  },
});

export default connect()(AddInstrumentModal);