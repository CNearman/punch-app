import React, {useState} from 'react';
import { StyleSheet, View } from 'react-native';
import { 
  Provider as PaperProvider, 
  DefaultTheme as PaperDefaultTheme, 
  DarkTheme as PaperDarkTheme, 
  Appbar, 
  useTheme, 
  Title,
  Caption,
  Paragraph,
  TouchableRipple,
  Drawer,
  Text,
  Switch,
  Modal,
  Portal,
  Divider
} from 'react-native-paper';

import {connect} from "react-redux";
import {
  DrawerItem,
  DrawerContentScrollView,
} from '@react-navigation/drawer';''
import AddInstrumentModal from './AddInstrumentModal'
import {selectInstrument} from '../redux/actions/selectInstrumentAction'


const DrawerContent = (props) => {
  const [modalOpen, setModalOpen] = useState(false);
  const hideModal = () => setModalOpen(false);
  return (
    <DrawerContentScrollView {...props} style= {{padding: 8}}>
      <Title style={{fontWeight: 'bold'}}>Punch</Title>
      <View style={{flex: 1, marginTop: 10}}>
        <Drawer.Section>
          <Text style={{marginTop:6, marginBottom: 10, fontSize: 18}}>Instruments</Text>
          <Divider/>
        {
          props.instruments.map((element, index) => {return(
            <TouchableRipple key={index} onPress={() => {
                props.dispatch(selectInstrument(index));
            }}>
              <View style={{paddingTop: 10, paddingBottom: 10}}>
                <Text>{element.name}</Text>
              </View>
            </TouchableRipple>
          )})
        }
        <TouchableRipple onPress={() => {setModalOpen(true)}}>
          <View style={{paddingTop: 10, paddingBottom: 10, marginTop: 20}}>
            <Text>New Instrument</Text>
          </View>
        </TouchableRipple>
        </Drawer.Section>
      </View>
      <Portal>
        <Modal visible={modalOpen} onDismiss={hideModal} contentContainerStyle={{backgroundColor: 'black', margin: 20}}>
          <AddInstrumentModal hideModal={hideModal}/>
        </Modal>
      </Portal>
    </DrawerContentScrollView>
  );
}

const mapStateToProps = (state) => {
  return {
    instruments: state.instrumentReducer.instruments
  };
};

export default connect(mapStateToProps)(DrawerContent)