import React from 'react';
import { View } from 'react-native';
import {Text, Surface} from 'react-native-paper';
import PropTypes from 'prop-types';

const DynamicStackInfo = (props) => {
	return(			
		<Surface style={{marginTop: 6}}>
			<View style={{flexDirection: 'row', justifyContent:'space-around'}}>
				<View style={{flexDirection:'column', padding: 8,  alignItems: 'center', justifyContent: 'center'}}>
					<Text>New Stack Size</Text>
					<Text style={{fontSize: 28}}>{props.stackSize}</Text>
				</View>				
				<View style={{flexDirection:'column', padding: 8, alignItems: 'center', justifyContent: 'center'}}>
					<Text>New Avg. Stack Price</Text>
					<Text style={{fontSize: 28}}>{props.averagePrice.toFixed(4)}</Text>
				</View>
			</View>
		</Surface>
	);
}

DynamicStackInfo.propTypes = {
	stackSize : PropTypes.number,
	averagePrice : PropTypes.number	
}

export default DynamicStackInfo;