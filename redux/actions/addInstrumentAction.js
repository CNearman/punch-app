import ActionTypes from '../actionTypes';

export const addInstrument = (instrumentDetails) =>(
    {
        type: ActionTypes.ADD_INSTRUMENT,
       
        instrumentDetails:{
        	name: instrumentDetails.name
        }
    });