import ActionTypes from '../actionTypes';

export const selectInstrument = (selectedInstrumentId) =>(
    {
        type: ActionTypes.SELECT_INSTRUMENT,
        selectedInstrumentId: selectedInstrumentId
    });