import ActionTypes from '../actionTypes';

export const setPunchDecimalPlaces = (punchDecimalPlaces) =>(
    {
        type: ActionTypes.SET_PUNCH_DECIMAL_PLACE,
       
        punchDecimalPlaces: punchDecimalPlaces
    });