import ActionTypes from '../actionTypes';
import 'react-native-get-random-values';
import { v4 as uuidv4 } from 'uuid';

export const savePunch = (punchDetails) =>(
    {
        type: ActionTypes.ADD_PUNCH,
       
        punchDetails:{
            amount:punchDetails.amount,
            price:punchDetails.price,
            instrumentId:punchDetails.instrumentId,
            id: uuidv4()
        }
    });