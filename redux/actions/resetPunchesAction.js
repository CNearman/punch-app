import ActionTypes from '../actionTypes';

export const resetPunches = (instrumentId) =>(
    {
        type: ActionTypes.RESET_PUNCHES,

        instrumentId: instrumentId
    });