import ActionTypes from '../actionTypes';

export const deletePunch = (punchId) => (
    {
        type: ActionTypes.DELETE_PUNCH,
        id: punchId
    });