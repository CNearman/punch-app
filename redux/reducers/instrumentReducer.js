import ActionTypes from "../actionTypes";

const initialState = { "instruments" : [{name: 'EUR/USD'}], "selectedInstrumentId" : 0 };

const instrumentReducer = (state = initialState, action) => {
	switch(action.type)
	{
		case ActionTypes.ADD_INSTRUMENT : {
			return { ...state, instruments: [...state.instruments, action.instrumentDetails] };
		}
		case ActionTypes.SELECT_INSTRUMENT : {
			return {...state, selectedInstrumentId : action.selectedInstrumentId};
		}
		default: {
			return state;
		}
	}
}

export default instrumentReducer;