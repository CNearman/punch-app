import ActionTypes from "../actionTypes";

const initialState = { "punches" : [] };

const punchStackReducer = (state = initialState, action) => {
	switch(action.type)
	{
		case ActionTypes.ADD_PUNCH : {
			return { ...state, punches: [...state.punches, action.punchDetails] };
		}
		case ActionTypes.RESET_PUNCHES : {
			return {...state, punches: [...state.punches.filter(punch => punch.instrumentId != action.instrumentId)]};
		}
		case ActionTypes.DELETE_PUNCH : {
			return {...state, punches: [...state.punches.filter(punch => punch.id != action.id)]};
		}
		default: {
			return state;
		}
	}
}

export default punchStackReducer;