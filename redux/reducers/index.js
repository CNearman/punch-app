import  {combineReducers} from 'redux';

import punchStackReducer from "./punchStackReducer";
import instrumentReducer from "./instrumentReducer";
import settingsReducer from "./settingsReducer";


const rootReducer = combineReducers({
	punchStackReducer : punchStackReducer,
	instrumentReducer : instrumentReducer,
	settingsReducer: settingsReducer
});

export default rootReducer;