import ActionTypes from "../actionTypes";

const initialState = { settings: { "punchDecimalPlaces" : 4 }};

const settingsReducer = (state = initialState, action) => {
	switch(action.type)
	{
		case ActionTypes.SET_PUNCH_DECIMAL_PLACE : {
			return { ...state, settings: {...state.settings, punchDecimalPlaces : action.punchDecimalPlaces} };
		}
		default: {
			return state;
		}
	}
}

export default settingsReducer;