import React from 'react';
import 'react-native-gesture-handler';
import { StyleSheet, Text, View } from 'react-native';

import { PersistGate } from 'redux-persist/es/integration/react';
import { Provider, TextInput } from 'react-redux';
import { 
  Provider as PaperProvider, 
  DefaultTheme as PaperDefaultTheme, 
  DarkTheme as PaperDarkTheme, 
} from 'react-native-paper';

import { store, persistor } from './redux/store/store';

import { NavigationContainer, DefaultTheme, DarkTheme} from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';

import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import StackScreen from "./screens/stackScreen"
import GoalScreen from "./screens/goalScreen";
import SettingsScreen from "./screens/settingsScreen";

import DrawerContent from "./components/DrawerContent";

const Tab = createMaterialBottomTabNavigator();
const Drawer = createDrawerNavigator();

const MainScreen = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen name="Stack" component={StackScreen} options={{tabBarIcon: ({color}) => (<MaterialCommunityIcons name="cash-multiple" color={color} size={26}/>)}} />
      <Tab.Screen name="Goal" component={GoalScreen} options={{tabBarIcon: ({color}) => (<MaterialCommunityIcons name="bullseye" color={color} size={26}/>)}} />
      <Tab.Screen name="Settings" component={SettingsScreen} options={{tabBarIcon: ({color}) => (<MaterialCommunityIcons name="tune" color={color} size={26}/>)}} />
    </Tab.Navigator>
  );
}


export default function App() {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <PaperProvider theme={PaperDarkTheme}>
          <NavigationContainer theme={DarkTheme}>
            <Drawer.Navigator drawerContent={() => <DrawerContent />}>
              <Drawer.Screen name="Main" component={MainScreen} />
            </Drawer.Navigator>
          </NavigationContainer>
        </PaperProvider>
      </PersistGate>
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

