import React from 'react';
import punchStackReducer from '../../redux/reducers/punchStackReducer';
import ActionTypes from '../../redux/actionTypes';

it('should delete a punch if one exists', () => {
	const initialState = {
		punches: [{amount: 1000, price: 2.1881, id: "don't_delete_me"}, {amount: 1000, price: 2.1881, id: "delete_me"} ]
	};

	const action = {
		type: ActionTypes.DELETE_PUNCH,
		id: "delete_me"
	}

	const result = punchStackReducer(initialState, action);

	expect(result.punches.length).toEqual(1);
	expect(result.punches[0].id).toEqual("don't_delete_me");
});

it('should not delete a punch if one doesn\'t exist', () => {
	const initialState = {
		punches: [{amount: 1000, price: 2.1881, id: "don't_delete_me"}, {amount: 1000, price: 2.1881, id: "don't_delete_me_too"} ]
	};

	const action = {
		type: ActionTypes.DELETE_PUNCH,
		id: "delete_me"
	}

	const result = punchStackReducer(initialState, action);

	expect(result.punches.length).toEqual(2);
	expect(initialState).toEqual(result);
});