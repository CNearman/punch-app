import React from 'react';
import * as deleteActions from '../../redux/actions/deletePunchAction';
import * as saveActions from '../../redux/actions/savePunchAction';
import * as resetActions from '../../redux/actions/resetPunchesAction';
import ActionTypes from '../../redux/actionTypes';

it('should create an action to delete a punch', () => {
	const testPunchId = "testPunchId";

	const expectedAction = {
		type: ActionTypes.DELETE_PUNCH,
		id: testPunchId
	}

	expect(deleteActions.deletePunch(testPunchId)).toEqual(expectedAction);
});


it('should create an action to add a punch', () => {
	const testPunchDetails = {price: 1.8111, amount: 1000};

	const expectedAction = {
		type: ActionTypes.ADD_PUNCH,
		price: 1.8111,
		amount: 1000
	};

	const result = saveActions.savePunch(testPunchDetails);

	expect(result.type).toEqual(expectedAction.type);
	expect(result.punchDetails.price).toEqual(expectedAction.price);
	expect(result.punchDetails.amount).toEqual(expectedAction.amount);
});

it('should create an action to reset all punches', () => {
	const expectedAction = {
		type: ActionTypes.RESET_PUNCHES
	}

	expect(resetActions.resetPunches()).toEqual(expectedAction);
});