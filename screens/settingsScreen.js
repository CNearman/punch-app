import React, {useState} from 'react';
import {SafeAreaView, View, TextInput, TouchableOpacity} from 'react-native'
import {Text, Appbar, Title, Divider} from 'react-native-paper';
import {connect} from "react-redux";
import {setPunchDecimalPlaces} from '../redux/actions/setPunchDecimalPlaces';
import InputSpinner from 'react-native-input-spinner';
import NumericInput from 'react-native-numeric-input'

const SettingsScreen = (props) => {
	const [settings, setSettings] = useState(props.settings)

	return (
		<SafeAreaView>
		    <Appbar.Header>
              <Appbar.BackAction onPress={() =>{props.navigation.toggleDrawer();}}/>
              <Appbar.Content title="Settings" />
            </Appbar.Header>
            <View style={{padding:8}}>
            	<Title>Settings</Title>
            	<Divider style={{marginTop: 4, marginBottom:16}}/>
            	<View style={{flexDirection:"row", alignItems: "center", justifyContent:"space-between"}}>
            		<View style={{flex: 1}}>
            			<Text style={{fontSize:18}}>Punch Decimal Places</Text>
            		</View>
		        	<View style={{flexDirection: "row", flex: 1, alignItems: "center"}}>
		        		<TouchableOpacity
		        			style={{backgroundColor:"#bb86fc", width:40, height:40, borderRadius: 20, alignItems:"center"}}
		        			onPress={() => {
		        				if (settings.punchDecimalPlaces > 0 && settings.punchDecimalPlaces <= 10) {
			        				setSettings({...settings, punchDecimalPlaces : settings.punchDecimalPlaces - 1});
			        				props.dispatch(setPunchDecimalPlaces(settings.punchDecimalPlaces - 1))
		        				}
		        			}}>
		        			<Text style={{color:"#000000", fontSize: 25}}>-</Text>
		        		</TouchableOpacity>
		        		<Text style={{color:"#FFFFFF", textAlign:"center", width: 60}}>{settings.punchDecimalPlaces}</Text>
		        		<TouchableOpacity
		        			style={{backgroundColor:"#bb86fc", width:40, height:40, borderRadius: 20, alignItems:"center"}}
		        			onPress={() => {
		        				if (settings.punchDecimalPlaces >= 0 && settings.punchDecimalPlaces < 10) {
			        				setSettings({...settings, punchDecimalPlaces : settings.punchDecimalPlaces + 1});
			        				props.dispatch(setPunchDecimalPlaces(settings.punchDecimalPlaces + 1))
			        			}
		        			}}
		        			> 
		        			<Text style={{color:"#000000", fontSize: 25}}>+</Text>
		        		</TouchableOpacity>
		    		</View>	
    			</View>
            </View>
		</SafeAreaView>
	);
}

const mapStateToProps = (state) => {
  return {
    settings: state.settingsReducer.settings
  };
};

export default connect(mapStateToProps)(SettingsScreen);