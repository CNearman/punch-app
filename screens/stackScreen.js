import React, {useState} from "react";
import {View, SafeAreaView, StyleSheet, TouchableHighlight, ScrollView} from "react-native";
import {TextInput, Text, HelperText, Button, DataTable, BottomNavigation, Divider, Surface, Title, FAB, Portal, Modal, Card, Appbar} from 'react-native-paper';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {connect, useSelector} from "react-redux";
import {savePunch} from '../redux/actions/savePunchAction';
import {resetPunches} from '../redux/actions/resetPunchesAction';
import {deletePunch} from '../redux/actions/deletePunchAction';

import AddPunchModal from '../components/AddPunchModal';
import CurrentStackData from '../components/CurrentStackData';
import ResetPunchesModal from '../components/ResetPunchesModal';

const itemsPerPage = 5;

const StackScreen = (props) => {
    let totalPunchCount = props.punches.reduce((accumulator, item) => { return accumulator + item.amount;}, 0);
    let totalCost = props.punches.reduce((accumulator, item) => { return accumulator + (item.price * item.amount);}, 0);

    const [page, setPage] = React.useState(0);
  	const from = Math.min(props.punches.length, (page * itemsPerPage) + 1);
  	const to = Math.min(props.punches.length, (page + 1) * itemsPerPage);

  	const [modalOpen, setModalOpen] = useState(false);
  	const hideModal = () => setModalOpen(false);

  	const [resetModalOpen, setResetModalOpen] = useState(false);
  	const hideResetModal = () => setResetModalOpen(false);
	return(
		<SafeAreaView>
		    <Appbar.Header>
              <Appbar.BackAction onPress={() =>{props.navigation.toggleDrawer();}} />
              <Appbar.Content title={props.currentInstrument.name}/>
            </Appbar.Header>
			<ScrollView style={styles.screenContainer}>
				<CurrentStackData  stackSize={totalPunchCount} averageStackPrice={(totalCost / totalPunchCount || 0).toFixed(props.settings.punchDecimalPlaces)}/>
				<Card style={{marginTop: 10, padding:10}}>
				<Title>Punches</Title>
				<DataTable style={styles.table}>
					<DataTable.Header>
						<DataTable.Title>Amount</DataTable.Title>
						<DataTable.Title>Price</DataTable.Title>
					</DataTable.Header>
					{
						props.punches.filter((element, index) => index+1 >= from && index + 1 <= to).map((element, index) => { return (
							<DataTable.Row key={index}>
								<DataTable.Cell>{element.amount}</DataTable.Cell>
								<DataTable.Cell>{element.price.toFixed(props.settings.punchDecimalPlaces)}</DataTable.Cell>
								<View style={{padding:8}}>
									<TouchableHighlight underlayColor="transparent" onPress={() => props.dispatch(deletePunch(element.id))}>
										<MaterialCommunityIcons name="delete" color="red" size={26}/>
									</TouchableHighlight>
								</View>
							</DataTable.Row>
						)})
					}
					<DataTable.Pagination
						page={page}
						numberOfPages={Math.ceil(props.punches.length / itemsPerPage)}
						onPageChange={page => setPage(page)}
						label={`${from}-${to} of ${props.punches.length}`}
					/>
				</DataTable>
				<Portal>
					<Modal visible={modalOpen} onDismiss={hideModal} contentContainerStyle={{backgroundColor: 'black', margin: 20}}>
						<AddPunchModal totalPunchCount={totalPunchCount} totalCost={totalCost} hideModal={hideModal} currentInstrumentId={props.currentInstrumentId}/>
					</Modal>
					<Modal visible={resetModalOpen} onDismiss={hideResetModal} contentContainerStyle={{backgroundColor: 'black', margin: 20}}>
						<ResetPunchesModal hideModal={hideResetModal} currentInstrumentId={props.currentInstrumentId}/>
					</Modal>
				</Portal>
				<TouchableHighlight underlayColor="transparent">
                	<Button style={{...styles.button}} mode="contained" 
                		onPress={() => {             			
                        	setResetModalOpen(true);
                 	}}>
                     	Reset
                 	</Button>
                </TouchableHighlight>
                </Card>
			</ScrollView>
			<Portal>
				<FAB
				    style={styles.fab}
				    small
				    icon="plus"
				    onPress={() => {setModalOpen(true)}}
				  />
		  	</Portal>
		</SafeAreaView>
	);
}



const styles = StyleSheet.create({
  button: {
  	width: 100
  },
  table: {
  	marginBottom: 10,
  },
  input: {
  	marginBottom: 10
  },
  inputContainer: {
  	flexDirection: "column",
  	justifyContent: 'space-around',
  },
  screenContainer: {
  	padding: 12,
  	flexDirection: 'column',
  },
  fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 54,
  },
});

const mapStateToProps = (state) => {
  return {
    punches: state.punchStackReducer.punches.filter((element) => element.instrumentId == state.instrumentReducer.selectedInstrumentId),
    currentInstrumentId: state.instrumentReducer.selectedInstrumentId || 0,
    currentInstrument: state.instrumentReducer.instruments[state.instrumentReducer.selectedInstrumentId || 0],
    settings: state.settingsReducer.settings
  };
};

const mapDispatchToProps = (dispatch) => {
	return {
		reduxDeletePunch: (punchId) => dispatch(deletePunch(punchId))
	}
}
    
export default connect(mapStateToProps)(StackScreen);