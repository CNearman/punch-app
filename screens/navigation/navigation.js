import { createStackNavigator, createAppContainer } from "react-navigation-stack";

import StackScreen from "../stackScreen"
import GoalScreen from "./screens/goalScreen";
import SettingsScreen from "./screens/settingsScreen";

const AppNavigator = createStackNavigator(
{
	stackScreen:{
		screen:StackScreen,
		navigationOptions: { header: null}
	},
	SettingsScreen:{
		screen:settingsScreen,
		navigationOptions: { header: null}
	},
	goalScreen:{
		screen:GoalScreen,
		navigationOptions: { header: null}
	}
});


export default createAppContainer(AppNavigator);