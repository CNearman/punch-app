import React, {useState, useEffect} from 'react';
import {SafeAreaView, View, TouchableHighlight, StyleSheet} from 'react-native';
import {Text, TextInput, HelperText, Button, Title, Divider, Card, Appbar} from 'react-native-paper';
import {connect} from "react-redux";
import {savePunch} from '../redux/actions/savePunchAction';

import CurrentStackData from '../components/CurrentStackData';

const validateIsNotANumber = (input) => {
	return isNaN(Number(input))
}

const GoalScreen = (props) => {
	const [goalPrice, setGoalPrice] = useState("");
	const [marketPrice, setMarketPrice] = useState("")
    let displayGoalPrice = Number(goalPrice) || 0;
    let displayMarketPrice = Number(marketPrice) || 0;

    let totalPunchCount = props.punches.reduce((accumulator, item) => { return accumulator + item.amount;}, 0);
    let totalCost = props.punches.reduce((accumulator, item) => { return accumulator + (item.price * item.amount);}, 0);

    const resetState = () =>{
		setGoalPrice("");
		setMarketPrice("");
	}

    let errors = [];
  	if (validateIsNotANumber(goalPrice)) {
  		errors.push('Goal Price');
  	}
  	if (validateIsNotANumber(marketPrice)) {
  		errors.push('Market Price');
  	}
  	let averageStackPrice = (totalCost / totalPunchCount || 0).toFixed(props.settings.punchDecimalPlaces);
  	let displayText = "";
  	let amountToBuy = Math.round((totalPunchCount * ( goalPrice - averageStackPrice))/(marketPrice - goalPrice));
  	if (amountToBuy == Infinity || amountToBuy < 0 || isNaN(amountToBuy)) {
  		displayText = "No way to reach your goal!";
  	} else {
  		displayText = `${amountToBuy} units`;
  	}
	return (
		<SafeAreaView>
		    <Appbar.Header>
              <Appbar.BackAction onPress={() =>{props.navigation.toggleDrawer();}} />
              <Appbar.Content title="Goal" />
            </Appbar.Header>
			<View style={styles.screenContainer}>
				<CurrentStackData stackSize={totalPunchCount} averageStackPrice={averageStackPrice}/>
				<View style={styles.inputContainer}>
					<Title>Goal</Title>
					<TextInput
						label="Goal Price"
						keyboardType= 'number-pad'
						numeric
						value={goalPrice}
						onChangeText={text => setGoalPrice(text)}
						error={validateIsNotANumber(goalPrice)}
						style={styles.input}
					/>
					<TextInput
						label="Market Price"
						keyboardType= 'number-pad'
						numeric
						value={marketPrice}
						onChangeText={text => setMarketPrice(text)}
						error={validateIsNotANumber(marketPrice)}
						style={styles.input}
					/>
					<HelperText type="error" visible={validateIsNotANumber(goalPrice) || validateIsNotANumber(marketPrice)}>
						{errors.join(' and ').concat(`${errors.length > 1 ? " are" : " is" } not number${errors.length > 1 ? "s" : "" }!`)}
					</HelperText>
					<View style={{flexDirection: 'row', justifyContent: 'center'}}>
						<Text style={{fontSize: 20}}>{displayText}</Text>
					</View>
					<View style={{flexDirection: 'row', justifyContent: 'flex-end'}}>
						<TouchableHighlight style={{marginTop:10}}underlayColor="transparent">
		        		<Button 
		        			mode="contained"
		        			style={{width:100}}
		        			onPress={() => {
	                    	if (isNaN(Number(amountToBuy))) {
	                    		return;
	                    	}
	                    	if (isNaN(Number(marketPrice))) {
	                    		return;
	                    	}
	                        var punchDetails = {};
	                        punchDetails.amount = amountToBuy;
	                        punchDetails.price = Number(marketPrice);
	                        punchDetails.instrumentId = props.currentInstrumentId
	                        console.log(JSON.stringify(punchDetails));
	                        props.dispatch(savePunch(punchDetails))
	                        resetState();
                 		}}>
		             		Save
		         		</Button>
		        	</TouchableHighlight>
					</View>
				</View>
			</View>
		</SafeAreaView>
	);
}

const styles = StyleSheet.create({
  inputContainer: {
  	flexDirection: "column",
  	justifyContent: 'space-around',
  },
  screenContainer: {
  	padding: 12,
  	flexDirection: 'column',
  },
  fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 54,
  },
  input: {
  	marginBottom: 10
  }
});

const mapStateToProps = (state) => {
  return {
    punches: state.punchStackReducer.punches.filter((element) => element.instrumentId == state.instrumentReducer.selectedInstrumentId),
    currentInstrumentId: state.instrumentReducer.selectedInstrumentId || 0,
    settings: state.settingsReducer.settings
  };
};

export default connect(mapStateToProps)(GoalScreen);